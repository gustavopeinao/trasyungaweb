import * as React from 'react';
import { MDBIcon } from "mdbreact";

// import carousel1 from './carousel1.jpg';
import instructivo from './img/instructivo.jpg'

function InstructivoPage () {
    
  return (
        <div style={{backgroundColor:"#D5E1A7"}}>
                <div className="col-md-10 offset-md-1" style={{paddingTop:60, paddingBottom:50}}>
                          
                        <div className="row">
                            <div className="col-md-7">
                                <h2 className="gobold h2-responsive font-weight-bold my-5 text-left" style={{borderBottom:"thick solid yellow", paddingBottom:20}}>Como Inscribirse</h2>
                                
                                    <div className="row" style={{paddingBottom:30}}>
                                        <div className="days col-md-3">
                                            <h1 style={{fontSize:100, color:"#11710B"}}>1</h1>
                                        </div>
                                        <div className="col-md-9">
                                            <h3 className="gobold font-weight-bold">Elije el Metodo de Pago</h3>
                                            <p>
                                                
                                                <b>1°.</b>A través de la plataforma podrás seleccionar las modalidad de pago, la cual podrá ser.<br></br>
                                                <b>  -</b>Pago en Efectivo en rapipago, pagofacil, Tarjeta de Débito, Tarjeta de Crédito, etc..<br></br> 
                                                <b>2°.</b>Una vez realizado el pago, deberás tomar nota del comprobante de pago y datos del titular de la tarjeta si correspondiere y proceder a completar el formulario de inscripción.<br></br>
                                            </p>
                                        </div>
                                    </div>
                                    <hr></hr>
                                    <div className="row" style={{paddingBottom:30}}>
                                        <div className="days col-md-3">
                                            <h1 style={{fontSize:100, color:"red"}}>2</h1>
                                        </div>
                                        <div className="col-md-9">
                                            <h3 className="gobold font-weight-bold">Completá el formulario de Inscripción</h3>
                                            <p>
                                                <b>3°.</b>Es muy importante que el correo electrónico que informes, sea el que usas habitualmente, porque es allí donde se te enviará información de interés-.<br></br>
                                                <b>4° </b>Completá todos los datos obligatorios en el formulario de inscripción con tus datos personales y los de tu pareja. Donde además podrás seleccionar el circuito y la categoría en la que participarán.<br></br>   
                                            </p>
                                        </div>
                                    </div>
                                    <hr></hr>
                                    <div className="row" style={{paddingBottom:30}}>
                                        <div className="days col-md-3">
                                            <h1 style={{fontSize:100, color:"red"}}>3</h1>
                                        </div>
                                        <div className="col-md-9">
                                            <h3 className="gobold font-weight-bold" style={{paddingBottom:30}}>Descraga y completa los siguientes Formularios</h3>
                                                <div className="row" style={{paddingBottom:30}}>
                                                    <h4 className="gobold font-weight-bold">Certificado de Aptitud Médica</h4>
                                                    <p>
                                                        El mismo deberá ser completado y firmado por un médico matriculado ya sea de forma privada o a través de algún organismo de salud pública.<br></br>
                                                        El mismo deberá ser presentado el día de la competencia para que se otorgue el número y/o pechera correspondiente.<br></br>
                                                        
                                                        
                                                    </p>
                                                    
                                                    <a href="https://drive.google.com/open?id=1BJ9g8ZbtAP8iJU3UUJel3xh0994tW020" target="_blank">
                                                        <h5>
                                                            <button className="btn btn-outline-danger btn-lg">
                                                            <MDBIcon far icon="file-pdf" className="mr-1" />
                                                            Descargar formulario en Pdf
                                                            
                                                            </button>
                                                        </h5>
                                                    </a>
                                                    
                                                </div>

                                                <div className="row">
                                                    <h4 className="gobold font-weight-bold">Autorización para Menores de 18 años</h4>
                                                    <p>
                                                    Los menores de 18 años para poder participar, deberán presentar la autorización firmada por los padres. El mismo deberá ser presentado, junto al certificado de aptitud médica el día de la competencia para que se le otorgue el número y/o pechera correspondiente.<br></br>
                                                        
                                                        
                                                    </p>
                                                    <a href="https://drive.google.com/open?id=1c0w7X2v9_9J2-vkd-k8Gv2QtGsuVakbU" target="_blank">
                                                        <h5>
                                                        <button className="btn btn-outline-danger btn-lg">
                                                            <MDBIcon far icon="file-pdf" className="mr-1" />
                                                            Descargar formulario en Pdf
                                                            
                                                            </button>
                                                        </h5>
                                                    </a>
                                                </div>
                                        </div>
                                    </div>
                                    <hr></hr>
                                    <p>
                                    No olvides el día de la competencia, en la mesa de acreditaciones, para poder recibir el número ambos competidores que conforman la pareja deberán presentar.<br></br>
                                        
                                    -Certificado de Aptitud Médica.<br></br>
                                    -Autorización de Menores (si correspondiere).<br></br>
                                    -Fotocopia del DNI.<br></br>
                                        
                                        
                                    </p>
                            </div>

                            <div className="col-md-5" style={{ paddingTop: 120 }}>
                                <img 
                                    src={instructivo} 
                                    width="400"  
                                    alt=""
                                />
                            </div>
                        </div>
                    
                        
                </div>
        </div>
  );
}

export default InstructivoPage;