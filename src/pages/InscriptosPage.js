import * as React from 'react';
//import desafio from "./img/desafio.jpg";
import DataTablePage from './DataTablePage';

function InscriptosPage () {
   
  return (
        <div style={{backgroundColor:"#D5E1A7"}}>  
            <div className="col-md-10 offset-md-1" style={{paddingTop:20, paddingBottom:50}}>     
                <div className="col-md-10 offset-md-1" style={{textAlign:"center"}}>
                    <h2 className="gobold h2-responsive font-weight-bold my-5 text-left" style={{borderBottom:"thick solid yellow", paddingBottom:20}}>Inscriptos</h2>
                    <DataTablePage />
                </div>            
            </div>
        </div>
  );
}

export default InscriptosPage;