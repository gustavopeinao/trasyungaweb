import * as React from 'react';

function HospedajePage () {
    
  return (
        <div>
                <div className="col-md-10 offset-md-1" style={{textAlign:"center", paddingBottom:50}}>
           
                    <h2 className="gobold h2-responsive font-weight-bold my-5 text-left" style={{borderBottom:"thick solid yellow", paddingBottom:20}}>Servicio de alojamiento - San Pedro de Jujuy</h2>

                        <div className="row ">

                            <div class="card text-center" style={{width: "17rem", margin:20}}>
                                <div class="card-body ">
                                    <h5 class="card-title">HOTEL SAMAY WASI</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Dirección: Vélez Sarsfield 85</h6>
                                    <p class="card-text">Contacto: 03888-426323/25</p>
                                    <p class="card-text">Plazas disponibles: 107</p>
                                    <p class="card-link">hotelsamaywasi@yahoo.com.ar</p>
                                    <p class="card-link" style={{padding:0, margin:0}}>www.hotelsamaywasi.com.ar </p>
                                </div>     
                            </div>
                            <div class="card text-center" style={{width: "18rem", margin:20}}>
                                <div class="card-body ">
                                    <h5 class="card-title">HOTEL RÍO GRANDE</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Dirección: Miguel Aráoz 452</h6>
                                    <p class="card-text">Contacto: 03888-426575</p>
                                    <p class="card-text">Plazas disponibles: 40</p>
                                    <p class="card-link">reservas@hotelriograndejujuy.com</p>
                                    <p class="card-link" style={{padding:0, margin:0}}>www.hotelriograndejujuy.com</p>
                                </div>     
                            </div>
                            <div class="card text-center" style={{width: "17rem", margin:20}}>
                                <div class="card-body ">
                                    <h5 class="card-title">HOTEL VELEZ SARSFIELD</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Dirección: Vélez Sarsfield 154</h6>
                                    <p class="card-text">Contacto: 03888-420446</p>
                                    <p class="card-text">Plazas disponibles: 68</p>
                                    <p class="card-link">Reservas: velezhotel@live.com</p>
                                </div>     
                            </div>
                            <div class="card text-center" style={{width: "17rem", margin:20}}>
                                <div class="card-body ">
                                    <h5 class="card-title">HOTEL CANDELA</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Dirección: Avda. 25 de Mayo 210</h6>
                                    <p class="card-text">Contacto: 03888-421321</p>
                                    <p class="card-text">Plazas disponibles: 23</p>
                                    <p class="card-link">Reservas: hotelcandela@arnet.com.ar</p>
                                </div>     
                            </div>
                            <div class="card text-center" style={{width: "17rem", margin:20}}>
                                <div class="card-body ">
                                    <h5 class="card-title">HOTEL S/C ABBY HOTEL</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Dirección: Suipacha 880 Bº Las Maderas</h6>
                                    <p class="card-text">Contacto: 03888-422799</p>
                                    <p class="card-text">Plazas disponibles: 34</p>
                                    <p class="card-link">Reservas: gracieladalal@hotmail.com</p>
                                </div>     
                            </div>
                            <div class="card text-center" style={{width: "17rem", margin:20}}>
                                <div class="card-body ">
                                    <h5 class="card-title">HOST 1* BELLAVISTA</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Direccion: Haití 110 esq. Chile</h6>
                                    <p class="card-text">Contacto: 03888-420213 - 03888-15562395</p>
                                    <p class="card-text">Plazas disponibles: 46</p>
                                    <p class="card-link">Reservas: hotelbellavista@hotmail.com</p>
                                </div>     
                            </div>
                            <div class="card text-center" style={{width: "17rem", margin:20}}>
                                <div class="card-body ">
                                    <h5 class="card-title">Res. “B” ALEX </h5>
                                    <h6 class="card-subtitle mb-2 text-muted">Direccion: Rogelio Leach 467</h6>
                                    <p class="card-text">Contacto: 03888-420526</p>
                                    <p class="card-text">Plazas disponibles: 44</p>
                                    <p class="card-link">Reservas: josefadelvalle@hotmail.com</p>
                                </div>     
                            </div>
                            
                            

                        </div>
             
                </div>
        </div>
  );
}

export default HospedajePage;