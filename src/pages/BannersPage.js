import * as React from 'react';
import {  MDBRow, MDBCol, MDBCard, MDBCardBody, MDBMask, MDBIcon, MDBView } from "mdbreact";
import "mdbreact/dist/css/mdb.css";
import "./FotosPage.css"
import ban1 from "./img/ban1.png";
import ban2 from "./img/ban2.png";
import ban3 from "./img/ban3.png";
import ban4 from "./img/ban4.png";


function BannersPage () {
   
  return (
        <div style={{padding:30}}>
            <div className="col-md-10 offset-md-1">
                          
                <div className="row" style={{paddingBottom:30}}>
                    <div className="col-md-12">
                        <h2 className="gobold h2-responsive font-weight-bold my-3 text-left" 
                            style={{borderBottom:"thick solid yellow", paddingBottom:20}}>
                            Publicidad Oficial
                        </h2>
                        <h4 className="gobold text-center" style={{padding:20}}>
                            Descargate los banners del evento haciendo click sobre el que mas te guste
                        </h4>
                        <div className="row">
                            <div className="col-md-6" style={{marginBottom: "1.5rem"}}>
                                   
                                    <div className="view rounded overlay z-depth-1-half" style={{height:"auto"}}>
                                        <img
                                            className="img-fluid"
                                            src={ban1}
                                            alt=""
                                        />
                                        <a href={ban1} download="DesafioTrasyunga-Banner1">
                                            <MDBMask overlay="white-slight" className="waves-light" />
                                        </a>
                                    </div>
                                
                            </div>
                            
                            <div className="col-md-6" style={{marginBottom: "1.5rem"}}>
                                
                                    <div className="view rounded overlay z-depth-1-half" style={{height:"auto"}}>
                                        <img
                                            className="img-fluid"
                                            src={ban2}
                                            alt=""
                                        />
                                        <a href={ban2} download="DesafioTrasyunga-Banner2">
                                            <MDBMask overlay="white-slight" className="waves-light" />
                                        </a>
                                    </div>
                                
                            </div>

                            <div className="col-md-6" style={{marginBottom: "1.5rem"}}>
                                
                                <div className="view rounded overlay z-depth-1-half" style={{height:"auto"}}>
                                    <img
                                        className="img-fluid"
                                        src={ban3}
                                        alt=""
                                    />
                                    <a href={ban3} download="DesafioTrasyunga-Banner3">
                                            <MDBMask overlay="white-slight" className="waves-light" />
                                    </a>
                                </div>
                            
                            </div>

                            <div className="col-md-6" style={{marginBottom: "1.5rem"}}>
                                
                                <div className="view rounded overlay z-depth-1-half" style={{height:"auto"}}>
                                    <img
                                        className="img-fluid"
                                        src={ban4}
                                        alt=""
                                    />
                                    <a href={ban4} download="DesafioTrasyunga-Banner4">
                                            <MDBMask overlay="white-slight" className="waves-light" />
                                    </a>
                                </div>
                            
                            </div>
                            


                        </div>
                    </div>

                    
                </div>  

            </div>
        </div>
  );
}

export default BannersPage;