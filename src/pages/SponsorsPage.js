import * as React from 'react';
//import desafio from "./img/desafio.jpg";
import spon1 from './img/spon1.png'
import spon2 from './img/spon2.png'
import spon3 from './img/spon3.png'
import spon4 from './img/spon4.png'
import spon5 from './img/spon5.png'
import spon6 from './img/spon6.png'
import spon7 from './img/spon7.png'
import spon8 from './img/spon8.png'
import spon9 from './img/spon9.png'
import spon10 from './img/spon10.png'
import spon11 from './img/spon11.png'
import spon12 from './img/spon12.png'
import spon13 from './img/spon13.png'
function SponsorsPage () {
   
  return (
        <div>       
            <div className="col-md-10 offset-md-1" style={{textAlign:"center", paddingBottom:50}}>
           
                <h2 className="gobold h2-responsive font-weight-bold my-5 text-left" style={{borderBottom:"thick solid yellow", paddingBottom:20}}>Sponsors</h2>
   
                    <div className="row">
                        
                        <div className="col-md-3">
                            <div className="container-fluid">
                                <img 
                                    
                                    src={spon1} 
                                    width="150px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid">
                                <img 
                                    
                                    src={spon2} 
                                    width="200px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid">
                                <img 
                                    
                                    src={spon3} 
                                    width="200px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        
                        <div className="col-md-3">
                            <div className="container-fluid" style={{paddingTop:40}}>
                                <img 
                                    
                                    src={spon4} 
                                    width="200px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid" style={{paddingTop:40}}>
                                <img 
                                    
                                    src={spon5} 
                                    width="150px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid">
                                <img 
                                    
                                    src={spon6} 
                                    width="150px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid" style={{paddingTop:40}}>
                                <img 
                                    
                                    src={spon7} 
                                    width="200px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid" style={{paddingTop:50}}>
                                <img 
                                    
                                    src={spon8} 
                                    width="200px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid" style={{paddingTop:70}}>
                                <img 
                                    
                                    src={spon9} 
                                    width="150px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid" style={{paddingTop:40}}>
                                <img 
                                    
                                    src={spon10} 
                                    width="150px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid" style={{paddingTop:40}}>
                                <img 
                                    
                                    src={spon11} 
                                    width="150px"
                                    alt=""
                                />
                            </div>   
                        </div>
                        <div className="col-md-3">
                            <div className="container-fluid" style={{paddingTop:40}}>
                                <img 
                                    
                                    src={spon12} 
                                    width="150px"
                                    alt=""
                                />
                            </div>   
                        </div>
    
                    </div>
                    
                    <div className="col-md-3 mx-auto">
                            <div className="container-fluid" style={{paddingTop:50}}>
                                <img 
                                    
                                    src={spon13} 
                                    width="200px"
                                    alt=""
                                />
                            </div>   
                        </div>
          
            </div>
                      
        </div>
  );
}

export default SponsorsPage;