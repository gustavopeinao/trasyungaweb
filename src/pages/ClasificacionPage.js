import * as React from 'react';
import clasificacion from "./img/clasificacion.jpg";

function ClasificaciónPage () {
    
  return (
        <div>
                <div className="col-md-10 offset-md-1" style={{paddingTop:60}}>
                          
                        <div className="row" style={{paddingBottom:30}}>
                            <div className="col-md-7">
                                <h2 className="gobold h2-responsive font-weight-bold my-5 text-left" style={{borderBottom:"thick solid yellow", paddingBottom:20}}>Clasificaciones 2019</h2>
                                
                                <p className="gobold">
                                LAs clasificaciones estarán disponibles a partir del 15 de Julio de 2019
                                </p>
                            </div>

                            <div className="col-md-5" style={{ paddingTop: 120 }}>
                                <img 
                                    src={clasificacion} 
                                    width="400"  
                                    alt=""
                                />
                            </div>
                        </div>
                    
                        
                </div>
        </div>
  );
}

export default ClasificaciónPage;