import * as React from 'react';
import "mdbreact/dist/css/mdb.css";
import "./FotosPage.css"
import gal1 from "./img/gal1.jpg";
import gal2 from "./img/gal2.jpg";
import gal3 from "./img/gal3.jpg";
import gal4 from "./img/gal4.jpg";
import gal5 from "./img/gal5.jpg";
import gal6 from "./img/gal6.jpg";
import gal7 from "./img/gal7.jpg";
import gal8 from "./img/gal8.jpg";

function FotosPage () {
   
  return (
        <div style={{padding:30}}>
            <div className="col-md-10 offset-md-1">
                          
                <div className="row" style={{paddingBottom:30}}>
                    <div className="col-md-12">
                        <h2 className="gobold h2-responsive font-weight-bold my-5 text-left" 
                            style={{borderBottom:"thick solid yellow", paddingBottom:20}}
                        >
                            Galería de Fotos
                        </h2>
                        <div className="row">
                            <ul className="galeria">
                                
                                {/* <div className="col-md-3" style={{padding:5}}>
                                    <img 
                                        src={gal8}
                                        className="img-fluid" 
                                        width="360"  
                                        alt=""
                                    />
                                </div> */}
                                <li><a ><img src={gal1} /></a></li>
                                <li><a ><img src={gal2} /></a></li>
                                <li><a ><img src={gal3} /></a></li>
                                <li><a ><img src={gal4} /></a></li>
                                <li><a ><img src={gal5} /></a></li>
                                <li><a ><img src={gal6} /></a></li>
                                <li><a ><img src={gal7} /></a></li>
                                <li><a ><img src={gal8} /></a></li>
                            </ul>
                        </div>
                    </div>

                    
                </div>  

            </div>
        </div>
  );
}

export default FotosPage;