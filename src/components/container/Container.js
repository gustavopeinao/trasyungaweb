import * as React from 'react';
import { BrowserRouter, HashRouter, Switch, Route } from "react-router-dom";
// import './App.css';
import Navbar from '../navbar/Navbar';
import Home from '../home/Home';
import Footer from '../footer/Footer';
import Desafio from '../desafio/Desafio';
import HistoryPage from '../desafio/HistoryPage'
import CircuitPage from '../desafio/CircuitPage'

import InstructivoPage from '../../pages/InstructivoPage'
// import InscripcionPage from '../../pages/InscripcionPage'
import InscriptosPage from '../../pages/InscriptosPage'
import Pago from '../pago/Pago'
import FotosPage from '../../pages/FotosPage'
import VideoBackground from '../VideoBackground/VideoBackground'
import SponsorsPage from '../../pages/SponsorsPage'
import BannersPage from '../../pages/BannersPage'
import ClasificacionPage from '../../pages/ClasificacionPage'
import HospedajePage from '../../pages/HospedajePage'


class Container extends React.Component {
   
  render() {
    return (
       <div>
           {/* <VideoBackground /> */}
           <div>
                <Navbar />
            </div>
            <div>
                {/* <HashRouter> */}
                    
                        <Route exact path='/' component={Home} />
                        <Route exact path='/desafio' component={Desafio} />
                        <Route exact path='/historia' component={HistoryPage} />
                        <Route exact path='/circuito' component={CircuitPage} />

                        <Route exact path='/instructivo' component={InstructivoPage} />
                        {/* <Route exact path='/inscripcion' component={InscripcionPage} /> */}
                        <Route exact path='/inscriptos' component={InscriptosPage} />
                        <Route exact path='/pago' component={Pago} />
                        <Route exact path='/fotos' component={FotosPage} />
                        <Route exact path='/videoBackground' component={VideoBackground} />
                        <Route exact path='/sponsorsPage' component={SponsorsPage} />
                        <Route exact path='/banners' component={BannersPage} />
                        <Route exact path='/clasificacion' component={ClasificacionPage} />
                        <Route exact path='/hospedaje' component={HospedajePage} />
                        
                {/* </HashRouter>   */}
            </div>
            <div>
                <Footer />  
            </div>
        </div>     
    );
  }
}

export default Container;