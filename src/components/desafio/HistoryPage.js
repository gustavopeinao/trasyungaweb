import * as React from 'react';
// import "./Header.css";
import flyerTras from "./flyerTras.jpg";
// import carousel1 from './carousel1.jpg';


function HistoryPage () {
    
  return (
        <div>
                <div className="col-md-10 offset-md-1" style={{paddingTop:60}}>
                          
                        <div className="row" style={{paddingBottom:30}}>
                            <div className="col-md-7">
                                <h2 className="gobold h2-responsive font-weight-bold my-5 text-left" style={{borderBottom:"thick solid yellow", paddingBottom:20}}>Historia del Trasyunga</h2>
                                
                                <p className="gobold">
                                Trasyunga nace como un sueño un 15 de marzo por la inquietud de un grupo de bikers locales deseosos de aventuras y por la falta de un desafio que distinga a su Región la Selva Jujeña, es por esto que de esta reunion salieron grandes ideas, entre ellas que el desafio sea en pareja y que lleve a los bikers por paisajes unicos, bellos y duros, con caracteristicas variadas  de xc y rally cross, asi de este modo el Trasyunga te llevará a internarte en la Selva Sampedreña para que la competencia sea entre el binomio y la Naturaleza.
Es por eso que quienes crucen la meta sentiran un sabor de victoria y disfrute sin importar la posicion de competencia.
Veni a vivir la aventura, veni al trasyunga que llego para quedarse!!!
                                </p>
                            </div>

                            <div className="col-md-5" style={{ paddingTop: 120 }}>
                                <img 
                                    src={flyerTras} 
                                    width="400"  
                                    alt=""
                                />
                            </div>
                        </div>
                    
                        
                </div>
        </div>
  );
}

export default HistoryPage;