import * as React from 'react';
import {MDBIcon} from 'mdbreact'

import desafio from "./desafio.jpg";
import reglamento from "./ReglamentoTrasyunga2019.pdf"


class Desafio extends React.Component {
    constructor() {
        super();
        this.state = {
          
        }
        this.handleMostrar = this.handleMostrar.bind(this);
        this.handleOcultar = this.handleOcultar.bind(this);
        this.handleDescargar = this.handleDescargar.bind(this);
        
      }

    componentDidMount(){
        document.getElementById('mostrarocultar').style.display="none";
    }  
    
    handleMostrar(){
        // alert("mostrar");
        document.getElementById('mostrarocultar').style.display="block";
    }
    handleOcultar(){
        // alert("ocultar");
        document.getElementById('mostrarocultar').style.display="none";
    }
    handleDescargar(){
        alert("descargar")
        // return <a href={reglamento} download="DesafioTrasyunga-Banner1"/>
    }

    render() {
   
        return (
            <div style={{padding:30}}>
                <div className="col-md-10 offset-md-1" style={{paddingTop:60}}>
                            
                    <div className="row" style={{paddingBottom:30}}>
                        <div className="col-md-7">
                            <h2 className="gobold h2-responsive font-weight-bold  text-left" style={{borderBottom:"thick solid yellow", paddingBottom:20}}>Reglamento Desafio Trasyunga</h2>
                            
                            <p>
                            <strong>Articulo I Organización</strong>
                            <ol>
                                <li>El Trasyunga está organizado por Team San Pedro MTB y auspiciado por la Municipalidad de San Pedro de Jujuy de la Provincia de Jujuy.</li>
                                <li>La organización asegura el encuadramiento deportivo de la competencia (marcación del circuito, cronometraje, etc.) y la asistencia de emergencias durante la misma.</li>
                                <li>La organización controlará a través de las autoridades asignadas, la aplicación de este reglamento por parte de los competidores el cual al momento de inscribirse declaran conocer en su totalidad y que tiene carácter definitivo e irreversible.</li>
                                <li>El circuito puede ser modificado por la Organización en función de factores climáticos o de fuerza mayor.</li>
                            </ol>
                            <strong>Articulo II Inscripciones</strong>
                            <ol>
                                <li>El Trasyunga se correrá por equipos integrados por dos corredores, varones y/o mujeres que se encuadren dentro de las categorías previstas en el Reglamento.</li>
                                <li>El coordinador se reserva el derecho de rechazar la inscripción de aquellos interesados en participar que tengan antecedentes antideportivos o sanciones disciplinarias.</li>
                                <li>Los derechos de inscripción comprenden:
                            - Gastos de organización<br></br>
                            - Premios de 1º al 5º para cada categoría.<br></br>
                            - Doscientas (200) medallas finish a los 200 primeros inscriptos primeros en llegar.<br></br>
                            - Jersey Campeón generales 23k y 43k.<br></br>
                            - Jerseys Sorteo.<br></br>
                            - El control deportivo de la prueba y los gastos de señalización del circuito, cronometraje, controles, etc.<br></br>
                            - Primeros auxilios en el circuito durante la carrera.<br></br>
                            - Provisión de elementos de identificación y control.</li>
                            </ol>
                            </p>
                            
                            <div id="mostrarocultar">
                                <p>
                                <strong>Articulo III La Carrera</strong>
                                <ol>
                                    <li>El día 14 de Julio de 2019 se realizará la largada del Trasyunga 43k partir de las 09:00 hrs. y del Trasyunga 23K a las 09:30 hrs desde el paraje designado por la organización e informado en la página de internet oficial. Los participantes deberán estar en dicho paraje una hora antes de su hora de largada.</li>
                                    <li>Cada equipo estará compuesto por dos (2) corredores.</li>
                                    <li><strong>Recorrido Trasyunga 43K:</strong> La carrera inicia en el lugar designado por la organización en el Predio del Circuito Tierra Brava, de la ciudad de San Pedro de Jujuy, Prime del circuito de motocross, salida a sendero hasta llegar a entrada del Cañadón, subida por sendero hasta llegar a Arroyo Las Pichanas, tomando sendero paralelo al río hasta Porton Amarillo previo dos cruces de arroyo. Ingreso a camino ancho con vuelta al Predio de Largada, donde se encuentra el primer abastecimiento.
                                A partir de allí se ingresa a sendero hasta Motel Génesis, pasando por Policía Montada con ingreso a Sendero en subida hasta el Cruce de los 4 Caminos, con ingreso a sendero hasta Bº 25 de Mayo, vuelta al sendero hasta bifurcación con Trasyunga 23k pasando por "El Santito", hasta el pie del Cerro La Cruz donde se encontrará el segundo abastecimiento, personal de Defensa Civil .
                                Se ingresa al Cerro la Cruz, hasta la cima bajando por sendero el Rio Saladillo, hasta tranquera para ingresar al Camino Vecinal que lleva hasta el cruce con Gasoducto, donde se encontrará el 3er Abastecimiento, Personal de Defensa Civil y Asistencia Mecánica Básica.
                                Se avanza por camino vecinal, hasta sendero que conecta nuevamente con el Gasoducto, para llegar hasta la pendiente "La Curina", descenso, cruce de Rio, para ingresar a Sendero hasta el Gasoducto hasta el sendero que lleva al camino de los cables de Alta Tensión, vuelta al camino vecinal con ingreso a sendero hasta el Predio de Circuito de Motocross donde se encuentra el Prime de Llegada.
                                Los corredores, por el solo hecho de inscribirse, declaran y aceptan conocer este recorrido, el que ha sido convenientemente señalizado, no pudiendo aducir extravío en los senderos u equívocos de camino para el computo de sus tiempos de carrera.</li>
                                    <li><strong>Cronometraje:</strong> para cada categoría la organización asignará un horario de salida, iniciando la carrera con la totalidad de inscriptos por cada categoría. Se comunicará el tiempo de distanciamiento entre tops de largada y de cada categoría. Para clasificar el tiempo de carrera será computado para el integrante del equipo que entre en segundo lugar.</li>
                                    <li><strong>Horario de Partida:</strong> A la hora de partida de cada equipo se procederá a su llamado y su posterior ubicación en la línea de largada. Aquel equipo que no concurra a la llamada, o lo haga solo uno de los integrantes, perderá su lugar, autorizándose su partida recién cuando largue la última categoría al final de la grilla.</li>
                                    <li><strong>Cómputo:</strong> Se computará como horario de salida aquel que tenía asignado originalmente para cada categoría. El horario de salida de la pareja que no concurrió en tiempo, queda libre y el equipo siguiente larga al horario asignado. Una vez largada íntegramente todas las categorías, largarán los equipos que no llegaron a tiempo. Una vez largado el último corredor se procederá al cierre del control de largada.</li>
                                    <li>Todos los equipos deberán pasar por los puestos descriptos en el punto III.3. El horario máximo de pasada por El Cruce del Gasoducto será hasta las 14:30 hrs. Luego de ese horario no se permitirá el acceso de corredores al tramo siguiente el que será cerrado, y deberán volver, con las indicaciones que le impartan los organizadores, al lugar de partida. Los equipos que arriben después de este horario a este paraje, no serán clasificados. Esta disposición se efectúa por razones de seguridad. El horario de cierre en la llegada a los efectos de determinar la clasificación para la premiación, será a las 16:45 hrs. Los equipos de rastrillaje y rescate con motos de Enduro partirán desde la Llegada y el Cruce del Gasoducto en forma simultánea, una hora después del cierre de estos puntos. A partir de esa hora, los enduristas recorrerán el circuito auxiliando a los participantes que no pudieran trasladarse por sus propios medios o en razón del horario sea prudente su traslado.</li>
                                    <li>En consonancia con el espíritu de esta prueba, los equipos deberán ser solidarios, no debiendo ir dispersos ni separados entre sus componentes. Si bien el espíritu de solidaridad es propio de la carrera, por cuestiones de seguridad y respeto a los demás participantes, esta prohibido tirarse / remolcarse uniendo las bicicletas con cualquier tipo de suplemento. En caso de que un miembro necesite ayuda el otro podrá ir empujándolo, unir las bicicletas para tirarse / remolcarse durante la carrera queda prohibido.</li>
                                    <li>La bicicleta de montaña (Mountain Bike) es el único medio válido de transporte durante la carrera, permitiéndose la marcha a pie cargando su bicicleta, o abandonándola en caso de deterioro irreparable (en tal caso la organización no se hace cargo de la misma), para arribar con el compañero, ya sea los dos en una misma máquina o alternándose en el uso de la misma. Los que no cumplan con esta reglamentación serán descalificados. Los dos corredores deberán llegar exhibiendo sus placas con el número de la pareja correspondiente.</li>
                                    <li><strong>E-Bike:</strong> Las bicicletas eléctricas de montaña o e-bike que son tipos de vehículos eléctricos consistente en una bicicleta a la que se le ha acoplado un motor eléctrico y/o de asistencia magnética para ayudar en el avance de la misma, no están permitidas.</li>
                                    <li><strong>Descalificaciones:</strong> Las autoridades de la Prueba descalificarán a los corredores que no cumplan con el presente reglamento. La organización se reserva el derecho de expulsar de la prueba a cualquier equipo que infrinja intencionalmente y/o reiteradamente el reglamento.</li>
                                    <li>El corredor participa bajo su única responsabilidad o de los padres o tutores, la organización no se responsabiliza por los robos, roturas o accidentes etc. antes, durante o después de la competencia. Se debe valorar la conveniencia considerando que es de alto riesgo.</li>
                                    <li>El circuito se traza en un marco natural, considerándose los obstáculos como accidentes propios de la naturaleza, de un circuito de Mountain Bike, liberando de toda responsabilidad a la Organización de los accidentes ocurridos por impericia o imprudencia, como también por fallas del rodado.</li>
                                </ol>
                                <strong>Articulo IV Participantes</strong>
                                <ol>
                                    <li>Cada equipo estará compuesto por dos participantes, cada uno con su bicicleta de montaña en excelentes condiciones de seguridad y funcionalidad, dado la exigencia del terreno.</li>
                                    <li>Cada competidor participa bajo su única responsabilidad. El debe valorar la conveniencia de largar y/o de continuar la prueba. No obstante, la Organización se reserva el derecho, siguiendo el consejo de un médico, de prohibir a un corredor a largar o continuar en competición, o no contar con el certificado de aptitud médica para participar.</li>
                                    <li>La organización no se hace responsable de las infracciones, por parte de los participantes o sus acompañantes, a la legislación vigente de las comunas que se atraviesa.</li>
                                    <li>Los corredores eximen a la Organización, a sus sponsors y colaboradores de reclamos o demandas en supuestas acciones u omisiones de los equipos, participantes, asistentes, agentes u otros actuantes de su favor.</li>
                                    <li>Los corredores eximen a la Organización, a sus sponsors, colaboradores y a sus agentes y empleados de cualquier reclamo o demanda resultante de un daño a un equipo, a sus materiales y propiedades, incluyendo, pero no limitando, pérdida o extravío, roturas, etc.</li>
                                </ol>
                                <strong>Articulo V: Equipamiento</strong>
                                <ol>
                                    <li>El Trasyunga es una competencia de autosuficiencia por lo que la ayuda externa no está permitida, salvo lo previsto por la Organización. Las comidas, bebidas y materiales necesarios para la prueba corren a cargo de los participantes y ellos deben procurar su traslado. No obstante la Organización preveerá puestos de abastecimiento de agua mineral y fruta en puntos estratégicos del recorrido</li>
                                    <li>Equipamiento obligatorio para la prueba. Cada corredor debe posee
                                - Bicicleta de Montaña (Mountain Bike) en perfectas condiciones de seguridad y funcionalidad.<br></br>
                                - Casco de ciclismo rígido. Es obligatorio el uso del mismo mientras dure la prueba. El incumplimiento será motivo de descalificación.<br></br>
                                - Tapones en el manubrio.<br></br>
                                - Número de equipo colocado en la parte delantera de la bicicleta, el que será provisto por la Organización. Esta se reserva los derechos exclusivos de las imágenes y publicidad de la prueba. La placa no se puede modificar por ningún motivo, siendo punible de no dejarlo largar en caso de realizarle alguna modificación por parte del corredor sin permiso de la organización.<br></br>
                                - Sus propias herramientas y repuestos, lo que pueden ser complementados entre los integrantes de los equipos. No se permitirá el cambio de bicicletas.<br></br>
                                - Si fuera posible, la organización colocará calcos identificatorias en el cuadro de la Bicicleta, las que deberán, obligatoriamente conservarse adheridas al cuadro hasta la llegada, y que serán verificadas por la organización antes de la premiación.</li><br></br>
                                    <li>Toda solución de problemas en las máquinas debe ser efectuada únicamente por los integrantes de cada equipo. Sin perjuicio de lo enunciado en el párrafo anterior, y siendo que ésta carrera promueve el espíritu solidario, se permite la asistencia y ayuda mecánica entre participantes de distintos equipos.</li>
                                    <li>La organización se reserva el derecho a denegar la salida a los equipos que no cumplan con este equipamiento y su calidad.</li>
                                    <li>La organización autoriza los puestos de "Asistencia mecánica" para brindar asistencia a los participantes de la prueba. Estarán ubicados en el Puesto de abastecimiento 3, tal cuals e enuncia en el punto III.3. Se trata de una asistencia rápida y ligera por parte de mecánicos especializados, no un taller de reparación. Tanto la organización como "el personal de asistencia mecánica" no se responsabilizan de la asistencia recibida, aunque se pretende que sea de la mejor calidad posible. La atención es libre y gratuita y queda a consideración de cada competidor.</li>
                                </ol>
                                <strong>Articulo VI Seguridad</strong>
                                <ol>
                                    <li>Las condiciones meteorológicas y otros imprevistos pueden obligar a la Organización a rediseñar el recorrido por motivos de seguridad.</li>
                                    <li>La Organización ubicará ambulancias de primeros auxilios en el Predio de Largada/Llegada complementando esta tarea con motos de enduro para el traslado y atención de corredores que necesiten atención o no puedan trasladarse por sus propios medios.</li>
                                    <li>Se garantiza a los corredores que el 100% del recorrido se efectuará por sendas y caminos agrícolas, no pavimentados. Queda totalmente prohibido la circulación de cualquier vehículo no autorizado o que no sea participante de la Organización. Las motos autorizadas para el control y seguridad, tendrán facultades para comunicar a la Coordinación sobre cualquier irregularidad, comportamiento antideportivo, etc. que observen en el desarrollo de la carrera.</li>
                                    <li>El público no podrá ingresar a ninguna parte del circuito y solo los verá pasar por la zona de Largada/Llegada, y puestos de hidratación.</li>
                                    <li>Todos los corredores menores de 18 años deberán presentar autorización expresa por escrito de sus padres / tutores para participar en este evento.</li>
                                    <li>Solo se garantizará la seguridad a los competidores en el recorrido oficial del circuito el día de la carrera en los horarios previstos de largada y hasta la finalización de entrega de premios. Los días anteriores y posteriores no se encuadran dentro del marco de los puntos 1 a 5 del presente artículo.-</li>
                                </ol>
                                <strong>Articulo VII Autoridades</strong>
                                <ol>
                                    <li>La organización designará los Directores de la prueba, teniendo autoridad para decidir sobre cualquier tema inherente a la misma.</li>
                                </ol>
                                <strong>Articulo VIII Trasyunga 23k</strong>
                                <ol>
                                    <li>Para las categorías del Trayunga 23k y la categoría Promocional, se organiza una competencia paralela denominada "Trasyunga 23k".</li>
                                    <li>En general rige el mismo reglamento que para el resto de las categorías, siendo el lugar de partida para esta prueba el mismo que el de Trasyunga, pero con un recorrido acortado detallado en el punto siguiente:</li>
                                    <li><strong>Recorrido Trasyunga 23k:</strong> La carrera inicia en el lugar designado por la organización en el Predio del Circuito Tierra Brava, de la ciudad de San Pedro de Jujuy, Prime del circuito de motocross, A partir de allí se ingresa a sendero hasta Motel Génesis, pasando por Policía Montada con ingreso a Sendero en subida hasta el Cruce de los 4 Caminos, con ingreso a sendero hasta Bº 25 de Mayo, vuelta al sendero hasta bifurcación con Trasyunga 43k, desviando por sendero hasta el Rio hasta sendero que conecta nuevamente con el Gasoducto, para llegar hasta la pendiente "La Curina", descenso, cruce de Rio, para ingresar a Sendero hasta el Gasoducto hasta el sendero que lleva al camino de los cables de Alta Tensión, vuelta al camino vecinal con ingreso a sendero hasta el Predio de Circuito de Motocross donde se encuentra el Prime de Llegada.</li>
                                    <li>El horario de revisación y marcado de máquinas, en parque cerrado será a partir de las 09:00 hrs., siendo el horario de partida las 10:00 hrs.</li>
                                </ol>
                                <strong>Articulo IX Categorías</strong>
                                <ol>
                                    <li>Las categorías serán las informadas en la página oficial del Trasyunga</li>
                                    <li>Cuando el equipo este compuesto por corredores de distintas categorías, será incluido en la categoría del corredor más competitivo, esto no siempre significa que sea el de menor edad de los 2.</li>
                                </ol>
                                Algunos ejemplos: cualquier Master que compitiera con un Master A, pasan automáticamente a Master A. En caso de que cualquiera de los participantes perteneciera a Elite corren en la categoría Elite, lo mismo para Sub23. Los únicos casos que quedan excluidos son los que forman parte de la categoria Mixta (*). Los casos de categorías Juveniles y PreJuveniles con Categorías Master tienen una definición especial, no siempre se corre en la categoría del más chico (menor edad) a pesar de que sea así en la mayoría de los casos, sobre todo cuando el Juvenil o PreJuvenil corre con una categoría Master que por sus características no tuviera diferencias competitivas, los cosas son pocos pero si se puede dar que puedan elegir cualquiera de las 2 categorías libremente de acuerdo a la matriz definida por Trasyunga. En Mixto la categoría la determina la edad de la dama. Se incluirán en las Categorías Promocionales a todos aquellos principiantes con poca experiencia en competencias de ciclismo de montaña. La organización se reserva el derecho de admisión a esta categoría una vez comprobados los antecedentes del corredor.

                                Solo en casos muy especiales y según la capacidad deportiva del competidor de acuerdo al conocimiento de la organización se permitirá la participación de menores de 11 años solamente en el <strong>Trasyunga 23k.</strong>

                                <strong>Articulo X Denuncias</strong>
                                <ol>
                                    <li>Las denuncias que efectúen los competidores sobre otros, serán recibidas hasta las 17:30 hrs. del día de la carrera, La denuncia se realizará por escrito en el formulario proporcionado por la organización, debiéndose acompañar la firma de dos corredores testigos, que la avalen junto al competidor denunciante. No se aceptarán denuncias de no corredores.</li>
                                </ol>
                                <strong>Articulo XI Premiación</strong>
                                <ol>
                                    <li>Los premios serán asignados de la siguiente manera:
                                - Para los Primeros puesto en la General del Trayunga 43k y 23k: Jersey Ganador<br></br>
                                - Primero al Quinto de cada categoría: Trofeos y Medallas<br></br>
                                - Cien primeras parejas en llegar: Medallas Finish<br></br>
                                - La categoría promocional se premiará a todos los participantes por igual pudiendo esto ser modificado según consideración de la organización. Todos reciben la misma medalla sin prioridad clasificatoria por ser la única categoría libre del Trasyunga</li><br></br>
                                </ol>
                                <strong>Articulo XII Consideraciones Generales</strong>
                                <ol>
                                    <li>Los competidores deberán cuidar de no hacer daño a la naturaleza, bajo el lema PACHAMAMA YO TE CUIDO, no arrojarán basura ni desperdicios de ningún tipo, ni harán daños en el circuito. Además guardarán respeto a los pobladores de los distintos lugares recorridos.</li>
                                    <li>Los competidores no deberán tener actitudes antideportivas tales como efectuar cortadas, obstruir el circuito, no permitir el sobrepaso de corredores, usar vocabulario inadecuado, gestos o cualquier demostración que vaya en contra de la educación y las buenas costumbres, así como faltar el respeto al público, organizadores y corredores de la prueba.</li>
                                </ol>
                                La Organización.-
                                </p>
                            </div>
                            <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-success active" onClick={this.handleMostrar}>
                                    <input type="radio" /> Leer Mas
                                </label>
                                <label class="btn btn-success" onClick={this.handleOcultar}>
                                    <input type="radio"  />Leer Menos
                                </label>
                                  
                            </div>
                            <a href={reglamento} download="DesafioTrasyunga-Reglamento2019">
                                    <label class="btn btn-outline-danger">
                                        <MDBIcon far icon="file-pdf" className="mr-1" />
                                        Desargar en pdf
                                    </label>        
                            </a> 
                        </div>

                        <div className="col-md-5" style={{ paddingTop: 60 }}>
                            <img 
                                src={desafio} 
                                width="400"  
                                alt=""
                            />
                            
                        </div>
                    </div>  

                </div>
            </div>
        );
    }
}

export default Desafio;