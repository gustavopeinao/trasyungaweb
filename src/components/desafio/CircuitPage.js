import * as React from 'react';
import circuito from "./circuito.jpg";
function CircuitPage () {
    
  return (
        <div style={{backgroundColor:"#D5E1A7"}}>
                <div className="col-md-10 offset-md-1" style={{paddingTop:60, paddingBottom:50}}>
                          
                        <div className="row">
                            <div className="col-md-7">
                                <h2 className="gobold h2-responsive font-weight-bold my-5 text-left" style={{borderBottom:"thick solid yellow", paddingBottom:20}}>El Circuito 2019</h2>
                                <h3 className="gobold h3-responsive font-weight-bold my-5 text-left">Dos Circuitos</h3>
                                <div className="row" style={{paddingBottom:30}}>
                                        <div className="days col-md-3">
                                            <h1 style={{fontSize:100, color:"#11710B"}}>43K</h1>
                                        </div>
                                        <div className="col-md-9">
                                            <p>
                                            La carrera inicia en el lugar designado por la organización en el Predio del Circuito Tierra Brava, de la ciudad de San Pedro de Jujuy, Prime del circuito de motocross, salida a sendero hasta llegar a entrada del Cañadón, subida por sendero hasta llegar a Arroyo Las Pichanas, tomando sendero paralelo al río hasta Porton Amarillo previo dos cruces de arroyo. Ingreso a camino ancho con vuelta al Predio de Largada, donde se encuentra el primer abastecimiento
                                            A partir de allí se ingresa a sendero hasta Motel Génesis, pasando por Policía Montada con ingreso a Sendero en subida hasta el Cruce de los 4 Caminos, con ingreso a sendero hasta Bº 25 de Mayo, vuelta al sendero hasta bifurcación con Trasyunga 23k pasando por "El Santito", hasta el pie del Cerro La Cruz donde se encontrará el segundo abastecimiento, personal de Defensa Civil .
                                            Se ingresa al Cerro la Cruz, hasta la cima bajando por sendero el Rio Saladillo, hasta tranquera para ingresar al Camino Vecinal que lleva hasta el cruce con Gasoducto, donde se encontrará el 3er Abastecimiento, Personal de Defensa Civil y Asistencia Mecánica Básica.
                                            Se avanza por camino vecinal, hasta sendero que conecta nuevamente con el Gasoducto, para llegar hasta la pendiente "La Curina", descenso, cruce de Rio, para ingresar a Sendero hasta el Gasoducto hasta el sendero que lleva al camino de los cables de Alta Tensión, vuelta al camino vecinal con ingreso a sendero hasta el Predio de Circuito de Motocross donde se encuentra el Prime de Llegada.
                                            </p>
                                            {/* <Link to="">
                                                <button type="button" class="btn btn-success btn-lg" style={{float:"right"}}>Visualizar Trazado Aquí</button>
                                            </Link> */}
                                        </div>
                                    </div>

                                    <div className="row" style={{paddingBottom:30}}>
                                        <div className="days col-md-3">
                                            <h1 style={{fontSize:100, color:"red"}}>23K</h1>
                                        </div>
                                        <div className="col-md-9">
                                            <p>
                                            La carrera inicia en el lugar designado por la organización en el Predio del Circuito Tierra Brava, de la ciudad de San Pedro de Jujuy, Prime del circuito de motocross,  A partir de allí se ingresa a sendero hasta Motel Génesis, pasando por Policía Montada con ingreso a Sendero en subida hasta el Cruce de los 4 Caminos, con ingreso a sendero hasta Bº 25 de Mayo, vuelta al sendero hasta bifurcación con Trasyunga 43k, desviando por sendero hasta el Rio hasta sendero que conecta nuevamente con el Gasoducto, para llegar hasta la pendiente "La Curina", descenso, cruce de Rio, para ingresar a Sendero hasta el Gasoducto hasta el sendero que lleva al camino de los cables de Alta Tensión, vuelta al camino vecinal con ingreso a sendero hasta el Predio de Circuito de Motocross donde se encuentra el Prime de Llegada.
                                            </p>
                                            {/* <button type="button" class="btn btn-danger btn-lg" style={{float:"right"}}>Visualizar Trazado Aquí</button> */}
                                        </div>
                                    </div>
                            </div>

                            <div className="col-md-5" style={{ paddingTop: 120 }}>
                                {/* <iframe src="https://www.google.com/maps/d/embed?mid=1FZnY0H28zED1iiyuxnDzKas_6yS5CJac&hl=es-419" width="500" height="700"></iframe> */}
                                <img 
                                    src={circuito} 
                                    width="400"  
                                    alt=""
                                />
                            </div>
                        </div>
                    
                        
                </div>
        </div>
  );
}

export default CircuitPage;