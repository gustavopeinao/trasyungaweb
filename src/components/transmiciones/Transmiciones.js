import * as React from 'react';
import DatatablePage from '../../pages/DataTablePage';


class Transmiciones extends React.Component {
    
  render() {
   
    return (
        <div className="my-5 px-5 mx-auto" style={{ fontWeight: 300, maxWidth: "90%" }}>
                <div  style={{ paddingTop: 0 }}>
                    <h2 className="h1-responsive font-weight-bold my-5 text-left">Inscripciones</h2>
                    
                        <nav>
                            <div class="nav nav-tabs justify-content-center" id="nav-tab" role="tablist">
                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Datos Personales</a>
                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Pago</a>
                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Lista de Inscriptos</a>
                            </div>
                        </nav>
                        <div class="tab-content justify-content-center" id="nav-tabContent">
                            {/* Datos personales */}
                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdEA5rmswlLT9ekBi6wCGyVTPuYBWDdu9ukdW249otO4VTPUA/viewform?embedded=true" width="500" height="800" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe>
                            </div>
                            {/* Pago */}
                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                <iframe src="https://www.mercadopago.com.ar/checkout/v1/redirect/f9870596-0529-45b2-9acc-ab8db68b73e6/payment-option-form/?preference-id=273142544-abcff687-bfd6-4c6e-b782-3462ca7e6ece#/" width="640" height="600" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe>
                            </div>
                            {/* Lista de inscriptos */}
                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                             <DatatablePage />
                            </div>
                        </div>

                        
                   
                </div>

      </div>
    );
  }
}

export default Transmiciones;