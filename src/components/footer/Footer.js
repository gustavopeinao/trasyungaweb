import * as React from 'react';
import { MDBCol, MDBContainer, MDBRow, MDBFooter } from "mdbreact";
import footerLogo1 from './footerLogo1.png';
import footerLogo2 from './footerLogo2.png';
import footerLogo3 from './footerLogo3.png';
import footerLogo4 from './footerLogo4.png';
import footerLogo5 from './footerLogo5.png';
import footerLogo6 from './footerLogo6.png';
import footerLogo7 from './footerLogo7.png';
import footerLogo8 from './footerLogo8.png';
import footerLogo9 from './footerLogo9.png';
import footerLogo10 from './footerLogo10.png';
import footerLogo11 from './footerLogo11.png';
// import footerLogo12 from './footerLogo12.png';


class Footer extends React.Component {

    render() {
        return (
           <MDBFooter className="font-small pt-4" style={{backgroundColor: "#11710B"}}>
            <MDBContainer fluid className="text-center text-md-left">
                <MDBRow>
                    <MDBCol md="12" className="text-center">
                    <img
                        src={footerLogo1}
                        width="130"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo2}
                        width="90"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo3}
                        width="120"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo4}
                        width="120"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo5}
                        width="90"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo6}
                        width="120"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo7}
                        width="170"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo8}
                        width="100"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo9}
                        width="100"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo10}
                        width="100"
                        alt="logo"
                        style={{paddingRight:30}}
                      />
                      <img
                        src={footerLogo11}
                        width="90"
                        alt="logo"
                      />
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
            <div className="footer-copyright text-center py-3" style={{marginTop: '20px',}}>
                <MDBContainer fluid>
                &copy; {new Date().getFullYear()} Copyright: <a href="#"> ABM - Seguridad Informática </a>
                </MDBContainer>
            </div>
    </MDBFooter>





                )
            }
        }
        
export default Footer;