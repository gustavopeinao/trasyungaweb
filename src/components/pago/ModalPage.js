import React, { Component } from 'react';
import { MDBContainer, MDBBtn, MDBModal, MDBModalBody, MDBModalHeader, MDBModalFooter } from 'mdbreact';

class ModalPage extends Component {
state={
  
  modal4: false,
  
}

toggle = nr => () => {
  let modalNumber = 'modal' + nr
  this.setState({
    [modalNumber]: !this.state[modalNumber]
  });
}

render() {
  return (
    <MDBContainer>
      
      
      
      
      <MDBBtn color="primary" onClick={this.toggle(4)}>Large modal</MDBBtn>
      <MDBModal isOpen={this.state.modal4} toggle={this.toggle(4)} size="lg">
        
        <MDBModalBody style={{padding:0}}>
        <div className=" card col-md-12 " style={{padding:0}}>
            <iframe className="col-md-12 col-sm.12" src="https://docs.google.com/forms/d/e/1FAIpQLSccb6DK-9oSvoHsrjmPROiGhLzjAI2bn6c74ui1_GRt1DPK0w/viewform?embedded=true" width="900" height="900" frameborder="0" marginheight="0" marginwidth="0">Cargando…</iframe>
        </div>    
        </MDBModalBody>
        
      </MDBModal>
      
    </MDBContainer>
    );
  }
}

export default ModalPage;