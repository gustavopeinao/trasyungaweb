import * as React from 'react';
//import "./Header.css";
import desafio from "./pago.jpg";
import mercadopago from "./mercadopago.jpg";
import presencial from "./presencial.jpg";
import ModalPage from "./ModalPage"
import ModalPageMP from "./ModalPageMP"

class Pago extends React.Component {

    

    render() {
        return (
            <div>
                <div className="col-md-10 offset-md-1" style={{ paddingTop: 30 }}>

                    <div className="row" style={{ paddingBottom: 30 }}>
                        <div className="col-md-7">
                            <h2 className="gobold h2-responsive font-weight-bold my-5 text-left" style={{ borderBottom: "thick solid yellow", paddingBottom: 20 }}>Inscripción</h2>
                            <p>
                                Es necesario pagar el arancel de inscripcón antes de completar el formulario.<br></br>
                                <b>1°</b> Si aún no realizaste el Pago, selecciona el botón "Pagar Inscripción"<br></br>
                                <b>2°</b> Si ya realizaste el pago y tienes los datos del comprobante de pago, selecciona el botón "Completar Formulario"
                            </p>
                            
                            <div className="row">
                                
                                <div className="col-md-6">

                                    <div class="card text-center" style={{ width: "20rem" }}>

                                        <img
                                            class="card-img-top"
                                            
                                            src={mercadopago}
                                            alt="Card image cap"
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">1° Pagar Inscripción</h5>
                                            <p class="card-text">Tarjeta de Credito/Debito. Hasta 12 cuotas SIN INTERES!. Rapipago / PagoFacil</p>
                                            <ModalPageMP />
                                            

                                        </div>
                                    </div>

                                </div>

                                <div className="col-md-6">

                                    <div class="card text-center" style={{ width: "20rem" }}>

                                        <img
                                            src={presencial}
                                            width="230"
                                            alt="Card image cap"
                                            style={{marginLeft:45}}
                                            
                                            
                                        />
                                        <div class="card-body">
                                            <h5 class="card-title">2° Completar formulario</h5>
                                            <p class="card-text">Ya realicé el pago<br></br>y tengo los datos del comprobante .</p>
                                            <ModalPage />
                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                        <div className="col-md-5" style={{ paddingTop: 120 }}>
                            <img
                                src={desafio}
                                width="400"
                                alt=""
                            />
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

export default Pago;