import * as React from 'react';
import "./Header.css";
import carousel1 from './carousel1.jpg';
import carousel2 from './carousel2.jpg';
import carousel3 from './carousel3.jpg';

function SliderPage () {
    
  return (
            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" style={{height: "90vh"}}>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img 
                        class="d-block w-100" 
                        src={carousel1} 
                        alt="First slide"
                    />
                </div>
                <div class="carousel-item">
                    <img 
                        class="d-block w-100" 
                        src={carousel2} 
                        alt="Second slide"
                    />
                </div>
                {/* <div class="carousel-item" >
                    <img 
                        class="view d-block w-100" 
                        src={carousel3} 
                        alt="Third slide"
                    />
                </div> */}
            </div>
        </div>

    
  )
}

export default SliderPage;