import * as React from 'react';
import '../../global.css';
//import { Link, BrowserRouter as Router } from "react-router-dom";
import { Link } from "react-router-dom";
import { MDBNavLink, MDBIcon } from "mdbreact";
import navbarLogo from './navbarLogo.png'

class Navbar extends React.Component {

  render() {
    return (
    
            <nav className="navbar navbar-expand-lg navbar-light" style={{backgroundColor: "#11710B"}}>
                <a className="navbar-brand" href="#" style={{paddingLeft:60, paddingTop:20, paddingBottom:15}}>
                    <img 
                        src={navbarLogo} 
                        width="180"  
                        alt=""
                    />   
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active" style={{paddingLeft:30}}>
                            <a className="gobold nav-link" href="#" style={{color:"white"}}>INICIO <span className="sr-only">(current)</span></a>
                        </li>
                        
                        <li className="nav-item dropdown">
                            <a className="gobold nav-link dropdown-toggle" style={{color:"white"}} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            DESAFIO
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <Link to="/historia" className="gobold dropdown-item">HISTORIA</Link>
                                <Link to="/circuito" className="gobold dropdown-item">RECORRIDOS</Link>
                                <Link to="/desafio" className="gobold dropdown-item">REGLAMENTO</Link>
                            </div>
                        </li>
  
                        <li className="nav-item dropdown">
                            <a className="gobold nav-link dropdown-toggle" style={{color:"white"}} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            INSCRIPCIÓN
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <Link to="/instructivo" className="gobold dropdown-item">INSTRUCTIVO</Link>
                                {/* <Link to="/inscriptos" className="gobold dropdown-item">INSCRIPTOS</Link> */}
                                <Link to="/pago" className="gobold dropdown-item">PAGAR</Link>
                            
                                {/* <a className="gobold dropdown-item" href="#">FORMULARIOS</a> */}
                            </div>
                        </li>
                        <li className="nav-item">
                            <Link to="/clasificacion" className="gobold nav-link" style={{color:"white"}}>CLASIFICACIONES</Link>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="gobold nav-link dropdown-toggle" style={{color:"white"}} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            MEDIOS
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <Link to="/fotos" className="gobold dropdown-item">FOTOS</Link>
                                {/* <a className="gobold dropdown-item" href="#">VIDEOS</a> */}
                                <Link to="/banners" className="gobold dropdown-item">PUBLICIDAD OFICIAL</Link>
                            </div>
                        </li>
                        <li className="nav-item dropdown">
                            <a className="gobold nav-link dropdown-toggle" style={{color:"white"}} href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            INFO ADICIONAL
                            </a>
                            <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                                <Link to="/sponsorsPage" className="gobold dropdown-item">SPONSORS</Link>
                                <Link to="/hospedaje" className="gobold dropdown-item">HOSPEDAJE</Link>
                            </div>
                        </li>
                    </ul>
                    {/* iconos rrss */}
                    {/* <MDBIcon fab icon="instagram" size="2x" className="white-text pr-3" style={{paddingLeft:30}}/> */}
                    <a href="https://www.facebook.com/Trasyunga-1926193964324445/" target="_blank">
                        <MDBIcon fab icon="facebook-square" className="white-text pr-3" size="2x" style={{paddingLeft:30}}/>
                    </a>
                    {/* <MDBIcon fab icon="whatsapp-square" className="white-text pr-3" size="2x" style={{paddingLeft:30}}/> */}
                    {/* <MDBIcon fab icon="twitter-square" className="white-text " size="2x" style={{paddingLeft:30, paddingRight:60}}/> */}
                </div>
            </nav>
        
    );
  }
}

export default Navbar;