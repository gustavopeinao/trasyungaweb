import React, { Component } from "react";
import { MDBMask, MDBRow, MDBCol, MDBIcon,   MDBBtn, MDBView, MDBContainer } from "mdbreact";
import "./VideoBackground.css";
import backgroundCicle from "./backgroundCicle.mp4"
import NavBarVB from "./NavBarVB"
import Footer from "../footer/Footer"


class VideoBackgroundPage extends Component {

  render() {
    
    return (
      <div id="videobackground">
      
        <NavBarVB />
        <div className="view">
          <video
            className="video-intro"
            
            playsInline
            autoPlay
            muted="muted"
            loop="loop"  
          >
            <source 
              src={backgroundCicle}
              type="video/mp4"
              height="50"
             />

          </video>
          <MDBMask className="d-flex justify-content-center align-items-center gradient">
            <div className="container">
              <div className="row">
                <div className="col-md-12 white-text text-center">

                  <h3 className="display-3 font-weight-bold">
                    Desafio Trasyunga{" "}
                  </h3>
                  <h4 className="display-4 font-weight-bold">
                    San Pedro 2019{" "}
                  </h4>
                  <hr className="hr-light"/>
                  <h4 className="subtext-header mt-2 mb-4">
                    Sitio Web en Construccion ...
                  </h4>
                  
                </div>
              </div>
            </div>
          </MDBMask>
        </div>

        <Footer />
      </div>
    );
  }
}

export default VideoBackgroundPage;