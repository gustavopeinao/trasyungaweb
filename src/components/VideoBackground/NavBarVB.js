import * as React from 'react';
import navbarLogo from './navbarLogo.png'

function NavBarVB () {
    
  return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light" style={{backgroundColor: "#11710B"}}>
                <a className="navbar-brand" href="#" style={{paddingLeft:60, paddingTop:20, paddingBottom:15}}>
                    <img 
                        src={navbarLogo} 
                        width="180"  
                        alt=""
                    />   
                </a>

                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    
                </div>
            </nav>
                

        </div>
  );
}

export default NavBarVB;